﻿using UnityEngine;
using System.Collections;
namespace MoreMountains.InfiniteRunnerEngine {
	public class correctZ : MonoBehaviour {
		public int zValue;

		// 
		void Start () {
			Invoke ("correctValueZ", 1);
		
		}
		
		void correctValueZ() {
			Vector3 temp = transform.position;
			temp.z = zValue;
			transform.position = temp;
		}
	}
}
