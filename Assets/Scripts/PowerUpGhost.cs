﻿using UnityEngine;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine {
	public class PowerUpGhost : PickableObject {
		private GameObject player;


		protected virtual void Start () {
			player = GameObject.FindWithTag("Player");
		}

		protected override void ObjectPicked () {
			GhostStart ();
			Invoke ("GhostEnd", 12);
		}

		void GhostStart() {			
			//Set Color to "Ghostlike"
			player.GetComponent<SpriteRenderer> ().color = new Color (1f,1f,1f,0.5f);
			//Deactivate Collider
			Physics2D.IgnoreLayerCollision(0, 12);

		}

		void GhostEnd() {
			//Set Color to normal
			player.GetComponent<SpriteRenderer> ().color = new Color (1f,1f,1f,1f);
			//Activate Collider
			Physics2D.IgnoreLayerCollision(0, 12, false);
		}

	}
}
