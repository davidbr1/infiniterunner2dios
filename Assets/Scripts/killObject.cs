﻿using UnityEngine;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine {
	public class killObject : MonoBehaviour {		
		public bool onlyDeactivate;
		public string[] TagNameToHit;
		public AudioClip explosionSound;
		public GameObject ObjectExplosion;
		public int pointsToAdd;
	


		//Destroy GameObject on hit and play audio and visuals in Coroutine

		void OnTriggerEnter2D(Collider2D other) {
			for (int i = 0; i < TagNameToHit.Length; i++) {
				if (other.tag == TagNameToHit[i]) {			
					StartCoroutine ("killIt");
				}
			}

		}

		IEnumerator killIt () {	
			GameManager.Instance.AddPoints (pointsToAdd);
			if (onlyDeactivate) {
				gameObject.SetActive (false);
			} else {
				Destroy (gameObject);
			}

			if (ObjectExplosion != null) {
				Instantiate (ObjectExplosion, transform.position, Quaternion.identity);
			}
			if (explosionSound != null) {				
				SoundManager.Instance.PlaySound (explosionSound, transform.position);
			}

			yield return new WaitForSeconds (1);


		} 


	}
}
