﻿using UnityEngine;
using System.Collections;
namespace MoreMountains.InfiniteRunnerEngine 
{
	public class sessionCounter : MonoBehaviour {

		// Use this for initialization
		void Start () {
			if (PlayerPrefs.HasKey ("sessionCounter")) {
				int newCount = PlayerPrefs.GetInt ("sessionCounter") + 1;
				PlayerPrefs.SetInt ("sessionCounter", newCount);
				Debug.Log ("New Count is now: " + PlayerPrefs.GetInt("sessionCounter"));
			} else {
				PlayerPrefs.SetInt ("sessionCounter", 1);
				Debug.Log("New Counter created " + PlayerPrefs.GetInt("sessionCounter"));

			}
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}
	}
}
