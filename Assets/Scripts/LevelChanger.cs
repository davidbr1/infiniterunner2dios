﻿using UnityEngine;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine {
	public class LevelChanger : MonoBehaviour {
		public GameObject cannonballSpawner;
		public GameObject patternSpawner;
		public float newMinTime;
		public float newMaxTime;
		public float waveStartTime;
		public float waveDuration;
		public float timeBetweenWaves;
		public float forceWaitTime;


		private float storedMinTime;
		private float storedMaxTime;

		//Get The CannonballSpawnerScript
		private CannonballSpawner cannonballSpawnerScript;
		private DistanceSpawner patternSpawnerScript;

		void Start() {
			cannonballSpawnerScript = cannonballSpawner.GetComponent<CannonballSpawner> ();
			patternSpawnerScript = patternSpawner.GetComponent<DistanceSpawner> ();
			storedMinTime = cannonballSpawnerScript.minSpawnTime;
			storedMaxTime = cannonballSpawnerScript.maxSpawnTime;

			InvokeRepeating("changeShootingTime", waveStartTime, timeBetweenWaves );

		}

		void changeShootingTime () {
			//Deactivate Pattern Spawner while wave is active
			patternSpawnerScript.Spawning = false;
			cannonballSpawnerScript.minSpawnTime = newMinTime;
			cannonballSpawnerScript.maxSpawnTime = newMaxTime;

			Invoke ("backToNormal", waveDuration);

		}

		void backToNormal () {	
			//Reactivate Pattern Spawner after wave and set cannonballSpawner values back to initial
			cannonballSpawnerScript.minSpawnTime = storedMinTime;
			cannonballSpawnerScript.maxSpawnTime = storedMaxTime;	
			StartCoroutine (forceWait ());

		}

		IEnumerator forceWait(){
			yield return new WaitForSeconds (forceWaitTime);
			patternSpawnerScript.Spawning = true;

		}
	}
}
