﻿using UnityEngine;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine {

	public class OutOfBoundsDestroy : MonoBehaviour {
		public float destroyTime;

		// Use this for initialization
		void Start () {
			Invoke ("DestroyIt", destroyTime);	
		}
		
		void DestroyIt () {
			Destroy (gameObject);			
		}
	}
}
