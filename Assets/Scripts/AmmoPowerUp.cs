﻿using UnityEngine;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine {
	public class AmmoPowerUp : PickableObject  {
		public bool isAmmoPowerup;
		public bool isRewindPowerUp;
		public int decreaseSpeedValue;
		public bool isShieldPowerUp;

		private CharacterShoot gunScript;





		void Start () {
			if (isAmmoPowerup) {
				gunScript = GameObject.FindGameObjectWithTag ("Gun").GetComponent<CharacterShoot> ();	
			} 
		}

		protected override void ObjectPicked() {
			if (isAmmoPowerup) {
				gunScript.gunHealth = 1f;
			} else if (isRewindPowerUp) {
				float actualSpeed = LevelManager.Instance.Speed;
				LevelManager.Instance.SetSpeed (actualSpeed / decreaseSpeedValue);
			} 
														
		}
	
	}
}
