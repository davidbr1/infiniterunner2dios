﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.InfiniteRunnerEngine
{	
	/// <summary>
	/// A rocket controller (the longer you press, the higher you go).
	/// </summary>
	public class Rocket : PlayableCharacter 
	{
		/// The force applied when pressing the main button
		public float FlyForce = 20f;
		/// The maximum velocity
		public float MaximumVelocity = 5f;
		public GameObject groundedEffect;
		//Particle System
		public ParticleSystem Jetpack;
		public float forceFieldDuration;
		public bool hasForceField;
		public AudioClip electrickShock;

		private GameObject forceField;
		private GameObject gun;
		private CharacterShoot gunScript;


		protected bool _boosting=false;

		protected override void Start() {
			_animator = GetComponent<Animator>();
			ParticleSystem.EmissionModule emissionModule = Jetpack.emission;
			emissionModule.enabled=false;

			forceField = GameObject.Find("forceField");
			gun = GameObject.Find ("Gun");
			gunScript = gun.GetComponent<CharacterShoot> ();
		


		}

		/// <summary>
		/// On Update
		/// </summary>
		protected override void Update ()
		{
			// we determine the distance between the ground and the Jumper
			ComputeDistanceToTheGround();
			// we send our various states to the animator.      
			UpdateAnimator ();		
			// if we're supposed to reset the player's position, we lerp its position to its initial position
			ResetPosition();
			// we check if the player is out of the death bounds or not
	        CheckDeathConditions ();
			//Start Particle Emissionter


 			Fly();
		}

		/// <summary>
		/// On fixed update
		/// </summary>
		protected virtual void FixedUpdate()
		{
			// we clamp the velocity
			if(_rigidbodyInterface.Velocity.magnitude > MaximumVelocity)
	         {
					_rigidbodyInterface.Velocity = _rigidbodyInterface.Velocity.normalized * MaximumVelocity;
	         }
		}

		/// <summary>
		/// When pressing the main action button for the first time we start boosting
		/// </summary>
		public override void RightStart()
		{
			_boosting=true;
			_animator.SetBool ("jetpack", true);

			//Start Particle Emission
			ParticleSystem.EmissionModule emissionModule = Jetpack.emission;
			emissionModule.enabled=true;
			GetComponent<AudioSource> ().Play();
		}

		/// <summary>
		/// When we stop pressing the main action button, we stop boosting
		/// </summary>
		public override void RightEnd()
		{
			_boosting=false;
			_animator.SetBool ("jetpack", false);
			//Start Particle Emission
			ParticleSystem.EmissionModule emissionModule = Jetpack.emission;
			emissionModule.enabled=false;
			GetComponent<AudioSource> ().Stop();

		}

		//Shoot Bullet with left tab
		public override void LeftStart() {
			gunScript.ShootOnHit ();
		}



		/// <summary>
		/// When the rocket is boosting we add a vertical force to make it climb. Gravity will handle the rest
		/// </summary>
		public virtual void Fly()
		{
			if (_boosting)
			{
				// we make our character jump
				_rigidbodyInterface.AddForce(Vector3.up * FlyForce * Time.deltaTime );
					
			}
		}	

		protected override void OnCollisionEnter2D(Collision2D other) {
			if (other.gameObject.tag == "Ground") {
				Instantiate (groundedEffect, transform.position, Quaternion.identity);
			}
		}

		//Count-Down the field duration Time (will be invoked every second in OnTriggerEnter below), if Time reaches 0, run Flicker Coroutine and end PowerUp
		void DecreaseFieldTime () {
			if (forceFieldDuration <= 0) {
				//Stop count down an run FlickerField (which flickers and ends the force field)
				StartCoroutine (FlickerField ());
				CancelInvoke ();
				//Otherwise keep counting down
			} else {
				forceFieldDuration -= 1;
			}

		}


		//Wenn der Spieler noch in keinem Force Field ist, aktiviere es
		//Force Field PowerUp
		void OnTriggerEnter2D(Collider2D coll) {
			//Wenn der SPieler mit dem Schild PowerUp kollidiert...
			if (coll.gameObject.tag == "shield") {
				//Überprüfe, ob er sich schon in einem Feld befindet. Wenn nicht...
				if (!hasForceField) {
					hasForceField = true;
					forceField.GetComponent<SpriteRenderer> ().enabled = true;
					forceField.GetComponent<CircleCollider2D> ().enabled = true;
					forceField.GetComponent<AudioSource> ().enabled = true;

					InvokeRepeating ("DecreaseFieldTime", 0, 1);

				} else {
					forceFieldDuration = 20f;
				}


			}			
		}


		IEnumerator FlickerField() {
				
			SoundManager.Instance.PlaySound (electrickShock, transform.position);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			//Flicker Effect
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds (0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds (1.5f);

			SoundManager.Instance.PlaySound (electrickShock, transform.position);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds (0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds (1.5f);

			SoundManager.Instance.PlaySound (electrickShock, transform.position);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds (0.05f);





			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			forceField.GetComponent<CircleCollider2D> ().enabled = false;
			forceField.GetComponent<AudioSource> ().enabled = false;
			hasForceField = false;
			forceFieldDuration = 20f;

		} 

		/*

		IEnumerator ActiveForceField() {

			yield return new WaitForSeconds (forceFieldDuration); 	
			SoundManager.Instance.PlaySound (electrickShock, transform.position);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			//Flicker Effect
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds (0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds (1.5f);

			SoundManager.Instance.PlaySound (electrickShock, transform.position);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds (0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds (1.5f);

			SoundManager.Instance.PlaySound (electrickShock, transform.position);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = true;
			yield return new WaitForSeconds(0.05f);
			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds (0.05f);





			forceField.GetComponent<SpriteRenderer> ().enabled = false;
			forceField.GetComponent<CircleCollider2D> ().enabled = false;
			forceField.GetComponent<AudioSource> ().enabled = false;

		} */

	}
}