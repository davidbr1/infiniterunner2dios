﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;

namespace MoreMountains.InfiniteRunnerEngine
{	
	/// <summary>
	/// Handles all GUI effects and changes
	/// </summary>
	public class GUIManager : MonoBehaviour 
	{
		/// the pause screen game object
		public GameObject PauseScreen;	
	    /// the game over screen game object
	    public GameObject GameOverScreen;
	    /// the object that will contain lives hearts
	    public GameObject HeartsContainer;
	    /// the points counter
	    public Text PointsText;
		/// the level display
		public Text LevelText;
		/// the countdown at the start of a level
		public Text CountdownText;
		/// the screen used for all fades
		public Image Fader;

		public GameObject NewRecordImage;

		private int backGround;
		private int scoreNow;
		private int highScore;
		private int sessionCount;
		private int playedTime;
		private int deathCounter;



		
		// singleton pattern
		static public GUIManager Instance { get { return _instance; } }
		static protected GUIManager _instance;
		public void Awake()
		{
			_instance = this;
		}

	

		
		/// <summary>
		/// Initialization
		/// </summary>
		public virtual void Initialize()
		{
			RefreshPoints();
	        InitializeLives();

	        if (CountdownText!=null)
	        {
				CountdownText.enabled=false;
			}
	    }

		/// <summary>
		/// Initializes the lives display.
		/// </summary>
	    public virtual void InitializeLives()
	    {

	    	if (HeartsContainer==null)
	    		return;

	    	// we remove everything inside the HeartsContainer
	        foreach (Transform child in HeartsContainer.transform)
	        {
	            Destroy(child.gameObject);
	        }
			
	        int deadLives = GameManager.Instance.TotalLives - GameManager.Instance.CurrentLives;
	        // for each life in the total number of possible lives you can have
	        for (int i=0; i < GameManager.Instance.TotalLives; i++)
	        {
	        	// if the life is already lost, we display an empty heart
	            string resourceURL = "";
	            if (deadLives>0)
	            {
	                resourceURL = "GUI/GUIHeartEmpty";
	            }
	            else
	            {
	            	// if the life is still 'alive', we display a full heart
	                resourceURL = "GUI/GUIHeartFull";
	            }
	            // we instantiate the heart gameobject and position it
	            GameObject heart = (GameObject)Instantiate(Resources.Load(resourceURL));
	            heart.transform.SetParent(HeartsContainer.transform, false);
	            heart.GetComponent<RectTransform>().localPosition = new Vector3(HeartsContainer.GetComponent<RectTransform>().sizeDelta.x/2 - i * (heart.GetComponent<RectTransform>().sizeDelta.x * 75f ), 0, 0);
	            deadLives--;
	        }
	    }

	    /// <summary>
	    /// Override this to have code executed on the GameStart event
	    /// </summary>
	    public virtual void OnGameStart()
	    {
	    	 
	    }

		//Move GUI Objects
		public virtual void MoveIn(GameObject target,Vector3 originalPosition, Vector3 direction, float duration)
		{
			target.transform.position=originalPosition + direction * 20f;

			StartCoroutine(MMMovement.MoveFromTo(target,target.transform.position,originalPosition,duration,0.1f));	    	
		}

		public virtual void MoveOut(GameObject target, Vector3 direction, float duration)
		{			
			StartCoroutine(MMMovement.MoveFromTo(target,target.transform.position,target.transform.position + direction * 20f ,duration,0.1f));	    	
		}
		
		/// <summary>
		/// Sets the pause.
		/// </summary>
		/// <param name="state">If set to <c>true</c>, sets the pause.</param>
		public virtual void SetPause(bool state)
		{
			PauseScreen.SetActive(state);
	    }
		
		/// <summary>
		/// Sets the countdown active.
		/// </summary>
		/// <param name="state">If set to <c>true</c> state.</param>
		public virtual void SetCountdownActive(bool state)
		{
			if (CountdownText==null) { return; }
			CountdownText.enabled=state;
		}
		
		/// <summary>
		/// Sets the countdown text.
		/// </summary>
		/// <param name="value">the new countdown text.</param>
		public virtual void SetCountdownText(string newText)
		{
			if (CountdownText==null) { return; }
			CountdownText.text=newText;
		}
		
		/// <summary>
		/// Sets the game over screen on or off.
		/// </summary>
		/// <param name="state">If set to <c>true</c>, sets the game over screen on.</param>
		public virtual void SetGameOverScreen(bool state)
		{
			
			Time.timeScale = 0;
			GameOverScreen.SetActive(state);
			Text gameOverScreenTextObject = GameOverScreen.transform.Find ("ScoreDisplay").GetComponent<Text> ();
			Text gameOverScreenHighscoreTextObject = GameOverScreen.transform.Find ("HighscoreDisplay").GetComponent<Text> ();
			Text newHighscoreTextObject = GameOverScreen.transform.Find ("HighscoreTitle").GetComponent<Text> ();
			bool newRecord = SingleHighScoreManager.SaveNewHighScore(GameManager.Instance.Points);

			//Count time played until game over and sum it up in player prefs
			float playtime = Time.timeSinceLevelLoad;
			int playtimeInt = (int)playtime;

			//If there some time has already been saved from previous session, add the current session's time
			if (PlayerPrefs.HasKey ("TimeInGame")) {
				int storedTime = PlayerPrefs.GetInt ("TimeInGame");
				int newStoredTime = storedTime + playtimeInt;
				PlayerPrefs.SetInt ("TimeInGame", newStoredTime);
			} else {
				//If not, create that variable in Player Prefs
				PlayerPrefs.SetInt ("TimeInGame", playtimeInt);
			}




			/*if (PlayerPrefs.HasKey ("TotalPlayTime")) {
				int newCount = PlayerPrefs.GetInt ("deathCounter") + 1;
				PlayerPrefs.SetInt ("deathCounter", newCount);
				Debug.Log ("New Count is now: " + PlayerPrefs.GetInt("deathCounter"));
			} else {
				PlayerPrefs.SetInt ("deathCounter", 1);
				Debug.Log("New Counter created " + PlayerPrefs.GetInt("deathCounter"));

			}*/
	        

			backGround = PlayerPrefs.GetInt ("background");
			playedTime = PlayerPrefs.GetInt ("TimeInGame");
			sessionCount = PlayerPrefs.GetInt ("sessionCounter");
			deathCounter = PlayerPrefs.GetInt ("deathCounter");
			scoreNow =(int) Mathf.Round(GameManager.Instance.Points);
			highScore = (int) Mathf.Round (SingleHighScoreManager.GetHighScore ());

			//Send Highscore plus background variable to Analytics
			Analytics.CustomEvent ("HighScore", new Dictionary<string, object> {				
				{"Background", backGround},
				{"Score", scoreNow},
				{"Highscore", highScore},
				{"PlayedTime", playedTime},
				{"SessionCounter", sessionCount},
				{"deathCounter", deathCounter}
			});

			string highScoreNewText="";

			if (newRecord)
			{
				NewRecordImage.GetComponent<CanvasGroup>().alpha=1;
				MoveIn(NewRecordImage,NewRecordImage.transform.position,Vector3.left,0.5f);
				highScoreNewText="NEW ";
			}

			if (gameOverScreenTextObject!= null)
			{
				gameOverScreenTextObject.text=" "+Mathf.Round(GameManager.Instance.Points);
			}

			if (gameOverScreenHighscoreTextObject!= null)
			{
				gameOverScreenHighscoreTextObject.text = " "+Mathf.Round(SingleHighScoreManager.GetHighScore()).ToString();
			}

			newHighscoreTextObject.text = highScoreNewText + "HIGHSCORE";

		}


			
		/// <summary>
		/// Sets the text to the game manager's points.
		/// </summary>
		public virtual void RefreshPoints()
		{
			if (PointsText==null)
				return;

			PointsText.text=GameManager.Instance.Points.ToString("000 000 000");	
		}
		
		/// <summary>
		/// Sets the level name in the HUD
		/// </summary>
		public virtual void SetLevelName(string name)
		{
			if (LevelText==null)
				return;

			LevelText.text=name;		
		}
		
		/// <summary>
		/// Fades the fader in or out depending on the state
		/// </summary>
		/// <param name="state">If set to <c>true</c> fades the fader in, otherwise out if <c>false</c>.</param>
		public virtual void FaderOn(bool state,float duration)
		{
			if (Fader==null)
			{
				return;
			}
			Fader.gameObject.SetActive(true);
			if (state)
				StartCoroutine(MMFade.FadeImage(Fader,duration, new Color(0,0,0,1f)));
			else
				StartCoroutine(MMFade.FadeImage(Fader,duration,new Color(0,0,0,0f)));
		}		

		/// <summary>
		/// Fades the fader to the alpha set as parameter
		/// </summary>
		/// <param name="newColor">The color to fade to.</param>
		/// <param name="duration">Duration.</param>
		public virtual void FaderTo(Color newColor,float duration)
		{
			if (Fader==null)
			{
				return;
			}
			Fader.gameObject.SetActive(true);
			StartCoroutine(MMFade.FadeImage(Fader,duration, newColor));
		}		

		protected virtual void OnEnable()
		{
			EventManager.StartListening("GameStart",OnGameStart);
		}

		protected virtual void OnDisable()
		{
			EventManager.StopListening("GameStart",OnGameStart);
		}
	}
}